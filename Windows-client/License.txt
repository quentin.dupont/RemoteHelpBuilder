This software uses files from different softwares and under different licences
and copyright (following files paths refer to the installation directory):
- PuTTY, under the MIT licence. Files are located under "PuTTY\" directory,
  and full licence and copyright in "PuTTY\LICENCE"
- UltraVNC, under the GPL version 2 or higher. Files are located under
  "UltraVNC\, and full licence and copyright in "UltraVNC\Readme.txt"
- RemoteHelpBuilder, under the GPL version 3. File is "RemoteHelp.vbs" and
  includes copyright information.
